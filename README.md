# serverless-json-api

This is still a WIP, pending documentation.

## License

The MIT License (MIT)

Copyright (c) Diosney Sarmiento<br>
Copyright (c) Jose Carlos Ramos<br>
Copyright (c) Vifros Corp.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

## To Process

Is expected to have in the environment these set:

    // export AWS_ACCESS_KEY_ID = "Your AWS Access Key ID"
    // export AWS_SECRET_ACCESS_KEY = "Your AWS Secret Access Key"
    // export AWS_REGION = "us-east-1"

You can set them using https://docs.aws.amazon.com/lambda/latest/dg/env_variables.html

* For v2 model relationships as the specs recommends.

* Add a way to get req params auto-magically.
