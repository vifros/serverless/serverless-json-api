# Release notes

## 0.0.21 (2021-04-29)

- Added support for `default` schema property instead of relaying on default behavior.
- Added support for `enforceDefault` instead of schema `forceDefault` due inconsistencies.

## 0.0.20 (2019-10-31)

- Removed `lodash.isArray` in favor of `Array.isArray`.
- Added support for client generated `id`s.

## 0.0.19 (2019-09-28)

- Removed unneeded debugging code.

## 0.0.18 (2019-09-25)

- Stripped spaces from `fields`.

## 0.0.17 (2019-09-25)

- Fixed `fields` support by adding to the inclusion list the `id` and the range key.

## 0.0.16 (2019-09-25)

- Added support for `$in` filter operator.
- Updated extras for its Symbol variant.
- Updated `dynamoose`.

## 0.0.15 (2019-09-03)

- Added Boolean to allowed schema types.

## 0.0.14 (2019-08-25)

- Added needed CORS header to allow all requests to an API.

## 0.0.13 (2019-08-25)

- Added needed CORS header to allow all requests to an API.

## 0.0.12 (2019-08-23)

- Added support for `list`s schema data types.

## 0.0.11 (2019-08-22)

- Added npm `start` task.
- Added support for schema maps data types.
- Added support for dynamoose `enum` validations.

## 0.0.10 (2019-08-18)

- Updating `serverless-http` to fix https://github.com/dougmoscrop/serverless-http/issues/117.

## 0.0.9 (2019-08-18)

- Changed expected media type back to `application/json` due `serverless-http` bug.
  See https://github.com/dougmoscrop/serverless-http/issues/117

## 0.0.8 (2019-08-17)

- Fixed credentials for AWS online environment.

## 0.0.7 (2019-08-13)

- Further updated dependencies.

## 0.0.6 (2019-08-11)

- Updated dependencies and make the needed adjustments to application code.

## 0.0.5 (2019-05-19)

- Get the DynamoDB port depending on environment.

## 0.0.4 (2019-05-04)

- Improved AWS credentials getter for the ORM.

## 0.0.3 (2019-05-04)

- Added new util method `JsonApi.defineExtraProperty`, to be used to mark nested resources.
- Added new `isFirstInChain` param at the end of all handlers, so in the handler is know if was a first handler in chain or not.
- Added support for custom validations in the ORM.
- Added new `JsonApiValidationError`.
- Added new `FailedCustomValidation` error message.
- Allow setting the source in `JsonApiNotFoundError`.

## 0.0.2 (2019-05-02)

- db/validations: Finish early in loop when attribute is unknown.
- orm: Get AWS access from the env process or throw error if not there, for online processes.

## 0.0.1 (2019-04-30)

- Added new specialized error `JsonApiNotImplementedError`.
- Relaxed model loader required attributes to allow for routes without ORM models.

## 0.0.0 (2019-04-28)

- Initial package publication.
