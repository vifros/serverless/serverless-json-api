import { expect } from 'chai';

import {
  contentType,
  EndpointTypes,
  mediaType
} from '../constants/jsonapi';

import { TestTypes } from '../constants/test-suite';
import { TestItem }  from '../interfaces/test-suite.interfaces';

export class TestSuite {
  // `supertest` instance since it was giving errors when was loaded here.
  api: any;

  constructor(api: any) {
    this.api = api;
  }

  public executeTests(tests: Set<TestItem>) {
    tests.forEach((test) => {
      this.processTest(test);
    });
  }

  private processTest(test: TestItem, baseUrl: string = '') {
    let url = ('path' in test)
              ? baseUrl + test.path
              : baseUrl;

    switch (test.type) {
      case TestTypes.Description: {
        describe(test.title, () => {
          if (test.children && test.children.size > 0) {
            test.children.forEach((childTest) => {
              this.processTest(childTest, url);
            });
          }
        });

        break;
      }
      case TestTypes.JsonApiTest: {
        it(test.title, (done) => {
          let statusCode = (test.expectations && test.expectations.status)
                           ? test.expectations.status
                           : 200;

          if (test.endpointType === EndpointTypes.Collection) {
            this
              .api
              .get(url)
              .set('Accept', mediaType)
              .expect('Content-Type', contentType)
              .expect(function (res) {
                const body = JSON.parse(res.text);

                expect(body).to.have.property('data');
                expect(body.data).to.have.lengthOf(1);

                body.data.forEach((item) => {
                  expect(item).to.have.keys([
                    'id',
                    'type',
                    'attributes',
                    'links'
                  ]);

                  expect(item.links).to.have.property('self').and.be.a('string');
                });

                if (test.expectations && test.expectations.custom) {
                  test.expectations.custom(expect, body);
                }
              })
              .expect(statusCode, done);
          }
          else if (test.endpointType === EndpointTypes.Resource) {
            this
              .api
              .get(url)
              .set('Accept', mediaType)
              .expect('Content-Type', contentType)
              .expect(function (res) {
                const body = JSON.parse(res.text);

                expect(body).to.have.property('data')
                            .and.have
                            .keys([
                              'id',
                              'type',
                              'attributes',
                              'links'
                            ]);

                expect(body.data.links).to.have.property('self').and.be.a('string');

                if (test.expectations && test.expectations.custom) {
                  test.expectations.custom(expect, body);
                }
              })
              .expect(statusCode, done);
          }
          else {
            done('Unknown test endpoint type.');
          }
        });

        break;
      }
      default: {
        throw new Error('Unknown test type.');
      }
    }
  }
}
