import { JSONAPI_EXTRA_SYMBOL } from '../constants/symbols';

export function getExtras(object: any): any {
  return object[JSONAPI_EXTRA_SYMBOL] || null;
}
