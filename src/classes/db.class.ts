import * as dynamoose         from 'dynamoose';
import * as JsonApiSerializer from 'json-api-serializer';
import * as validator         from 'validator';
import isPlainObject          from 'lodash.isplainobject';
import mergeWith              from 'lodash.mergewith';

import {
  ORM_SCHEMA_DEFAULT_OPTIONS,
  ORM_SCHEMA_DEFAULT_PROPERTY_EXCLUSION_ATTRIBUTES_LIST,
  ValidationDirection
} from '../constants/db';

import {
  IDbModelLoaderMap,
  IOrm,
  IOrmExecutedQueryMap
} from '../interfaces/db.interfaces';

import { defaultSerializerOptions }    from '../constants/jsonapi';
import { ErrorMessages, ErrorTypes }   from '../constants/errors';
import { JsonApiError }                from './errors/jsonapi-error.class';
import { JsonApiNotFoundError }        from './errors/jsonapi-not-found-error.class';
import { ValidationError }             from './errors/validation-error.class';
import { ValidationErrors }            from './errors/validation-errors.class';
import { JsonApi }                     from './jsonapi.class';
import { DbSchemaInitializationError } from './errors/db-schema-initialization-error.class';

export class Db {
  orm: IOrm;
  models: Map<string, any>                     = new Map();
  modelLoaders: Map<string, IDbModelLoaderMap> = new Map();

  serializer = new JsonApiSerializer();

  constructor() {
    let credentials: any = {
      region: process.env.AWS_REGION || 'us-east-1'
    };

    if (process.env.IS_OFFLINE) {
      credentials.accessKeyId     = 'AKIAIOSFODNN7EXAMPLE';
      credentials.secretAccessKey = 'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY';
    }

    dynamoose.AWS.config.update(credentials);

    if (process.env.IS_OFFLINE === 'true') {
      // TODO @diosney: Get it from the `serverless.yml` or `env`.
      let port = (process.env.SERVICE_STAGE && process.env.SERVICE_STAGE === 'local')
                 ? '9000'
                 : '9001';

      dynamoose.local(`http://localhost:${port}`);
    }

    this.orm = dynamoose;
  }

  getModelByLoader(modelLoader: IDbModelLoaderMap): any {
    return this.models.get(modelLoader.id);
  }

  registerModels(modelLoaderMap: Set<IDbModelLoaderMap>) {
    if (!modelLoaderMap || modelLoaderMap.size <= 0) {
      return;
    }

    modelLoaderMap.forEach((modelLoader: IDbModelLoaderMap) => {
      if (modelLoader.schema
          && modelLoader.schemaOptions
          && modelLoader.tableName) {

        this.checkIfAllAttributesHaveTypesOrThrowError(modelLoader.schema, modelLoader.id);
        let schemaOptions = Object.assign({}, ORM_SCHEMA_DEFAULT_OPTIONS, modelLoader.schemaOptions);
        let schema: any   = new this.orm.Schema(modelLoader.schema, schemaOptions);

        this.registerStaticMethods(schema, modelLoader.schema, modelLoader);

        let model = this.orm.model(modelLoader.tableName, schema);
        this.models.set(modelLoader.id, model);
        this.modelLoaders.set(modelLoader.id, modelLoader);
      }

      this.serializer.register(modelLoader.id, Object.assign({}, defaultSerializerOptions, modelLoader.serializerOptions));
    });
  }

  checkIfAllAttributesHaveTypesOrThrowError(schema: any, sourcePrefix = '') {
    for (let attribute in schema) {
      if (!schema.hasOwnProperty(attribute)) {
        continue;
      }

      const source = `${sourcePrefix}${(sourcePrefix) ? '/' : ''}${attribute}`;

      if (!schema[attribute].hasOwnProperty('type')) {
        throw new DbSchemaInitializationError(source, ErrorMessages.MissingRequiredTypeInSchemaAttribute
                                                                   .replace('{source}', source));
      }

      const schemaType: any = schema[attribute].type;

      if (typeof schemaType === 'function') {
        if (['Number', 'String', 'Date', 'Boolean'].indexOf(schemaType.name) === -1) {
          throw new DbSchemaInitializationError(source, ErrorMessages.UnsupportedTypeInSchemaAttribute
                                                                     .replace('{source}', source));
        }
        continue;
      }
      if (typeof schemaType === 'string') {
        if (schemaType === 'map') {
          this.checkIfAllAttributesHaveTypesOrThrowError(schema[attribute].map, source);
          continue;
        }
        else if (schemaType === 'list') {
          const listItemSchema = schema[attribute].list[0];

          if (!isPlainObject(listItemSchema)) {
            throw new DbSchemaInitializationError(`${source}/0`, ErrorMessages.InvalidSchema
                                                                              .replace('{source}', `${source}/0`));
          }

          this.checkIfAllAttributesHaveTypesOrThrowError(schema[attribute].list, source);
          continue;
        }
        else {
          throw new DbSchemaInitializationError(source, ErrorMessages.UnsupportedTypeInSchemaAttribute
                                                                     .replace('{source}', source));
        }
      }

      throw new DbSchemaInitializationError(source, ErrorMessages.UnsupportedTypeInSchemaAttribute
                                                                 .replace('{source}', source));
    }
  }

  registerStaticMethods(schema: any, schemaObject: any, modelLoader: IDbModelLoaderMap): any {
    let self = this;

    let createOptions = {
      overwrite: false
    };

    schema.statics.xCreate = async function (object, options = createOptions): Promise<any> {
      let objectWithDefaults = await self.validate(schemaObject, object);
      const instance         = new this(objectWithDefaults);

      let response: any;

      try {
        response = await instance.save(options);
      }
      catch (e) {
        if (e.name === ErrorTypes.ConditionalCheckFailedException) {
          throw new JsonApiError(
            JsonApi.statusCodes.forbidden.id,
            JsonApi.statusCodes.forbidden.text,
            ErrorMessages.ClientGeneratedIdAlreadyExists,
            {
              pointer: '/data/id'
            });
        }
        throw e;
      }

      return response;
    };

    schema.statics.xGet = async function (key, options = {}): Promise<any> {
      return this.get(key, options);
    };

    // `update: true` needed to trigger an error if item was not found and use that for a 404 response.
    schema.statics.xDelete = async function (key, optOptions?: any): Promise<any> {
      let options = Object.assign({}, optOptions, {
        update: true
      });

      return this.delete(key, options);
    };

    // Important! It expects an `id` property on the key object!
    schema.statics.xUpdate = async function (key, values, optOptions = {}): Promise<any> {
      let valuesWithDefaults = await self.validate(schemaObject, values, {
        direction: ValidationDirection.DataToSchema
      });

      // Dynamoose replaces maps instead of updating its values, so an extra Get is needed for now.
      // See https://github.com/dynamoosejs/dynamoose/issues/665
      const valuesHasMaps = Object
        .keys(valuesWithDefaults)
        .some((attribute: string) => {
          return schemaObject[attribute].type === 'map';
        });

      if (valuesHasMaps) {
        let item = await this.xGet(key);

        if (!item) {
          throw new JsonApiNotFoundError();
        }

        Object
          .keys(schemaObject)
          .forEach((attribute: any) => {
            if (schemaObject[attribute].type === 'map') {
              valuesWithDefaults[attribute] = mergeWith({}, item[attribute], valuesWithDefaults[attribute], (objValue, srcValue) => {
                if (Array.isArray(objValue)) {
                  return srcValue;
                }
              });
            }
          });
      }

      let condition = Object
        .keys(key)
        .map((key) => {
          return `${key} = :${key}`;
        })
        .join(',');

      let options = Object.assign({}, optOptions, {
        // Needed to trigger an error if item was not found and use that for a 404 response.
        condition      : condition,
        conditionValues: key
      });

      return this.update(key, valuesWithDefaults, options);
    };

    schema.statics.xGetAll = async function (optQuery: any) {
      // TODO @diosney: Implement `first`, `last`, `previous` links, and a proper `null` next (now a link to an empty collection is sent).
      // TODO @diosney: Add `meta/page/cursor` to each item?
      let valuesToReturn: any[] = [];

      let reqQuery = Object.assign({}, optQuery);
      let lastKey  = null;

      // Optimized pagination by increasing the limit to 25 and then build the lastKey manually.
      let ormQueryMap = JsonApi.buildOrmQueryObjectFromReq(this, modelLoader.id, reqQuery, schemaObject);
      let ormQuery: any;

      while (valuesToReturn.length < ormQueryMap.reqQuery.page.size) {
        ormQuery = ormQueryMap.ormQuery;

        let intermediateValues: IOrmExecutedQueryMap = await ormQuery.exec();

        if (intermediateValues.length <= 0) {
          if (intermediateValues.lastKey) {
            // We can continue querying to fulfill page size.
            lastKey = intermediateValues.lastKey;
          }
          else {
            // There is no more items to query for.
            lastKey = null;
            break;
          }
        }
        else {
          // There are items to process.
          let missingQuantity = parseInt(ormQueryMap.reqQuery.page.size) - valuesToReturn.length;

          if (missingQuantity === 0) {
            // We should not never get here since below we break early when the condition is fulfilled.
            // TODO @diosney: Throw an error here? or at least log it uh?
            break;
          }
          else if (missingQuantity < 0) {
            // More items than page size?!
            // WE should not never get here since we add exactly the items we need, never more.
            // TODO @diosney: Throw an error here? or at least log it uh?
            break;
          }

          let slicedItemsToInsert = intermediateValues.slice(0, missingQuantity);
          let slicedItemsLastItem = slicedItemsToInsert[slicedItemsToInsert.length - 1];

          valuesToReturn.push(...slicedItemsToInsert);

          if (valuesToReturn.length === ormQueryMap.reqQuery.page.size) {
            lastKey = self.buildLastKeyFromItem(slicedItemsLastItem, modelLoader.id, ormQueryMap.rangeKey, schemaObject);
            break;
          }
          // `valuesToReturn < reqQuery.page.size` since above checked with `missingQty` that values are not greater than page size.
          if (slicedItemsToInsert.length > 0) {
            lastKey = self.buildLastKeyFromItem(slicedItemsLastItem, modelLoader.id, ormQueryMap.rangeKey, schemaObject);
          }
          else if (intermediateValues.lastKey) {
            lastKey = intermediateValues.lastKey;
          }
          else {
            lastKey = null;
            break;
          }
        }

        // If we get here then another query is needed to fulfill the request size.
        ormQueryMap = JsonApi.buildOrmQueryObjectFromReq(this, modelLoader.id, Object.assign({}, reqQuery, {
          page: {
            after: Buffer.from(JSON.stringify(lastKey)).toString('base64'),
            size : ormQueryMap.reqQuery.page.size
          }
        }), schemaObject);
      }

      JsonApi.addExtras(valuesToReturn, {
        links: {
          next: (lastKey !== null) ? Buffer.from(JSON.stringify(lastKey)).toString('base64') : null
        }
        // TODO @diosney: Implement `total` or `estimatedTotal`.
        // meta : {
        //   page: {
        //     total: 25,
        //     estimatedTotal: {
        //       bestGuess: 24
        //     }
        //   }
        // }
      });

      return valuesToReturn;
    };
  }

  buildLastKeyFromItem(item: any, type: string, rangeKey: string, schemaObject): any {
    return {
      type      : {
        S: type
      },
      id        : {
        S: item.id
      },
      [rangeKey]: {
        [this.getDynamoDbTypeFromSchemaAttribute(schemaObject[rangeKey])]: this.parseDynamoDbTypeFromSchemaAttribute(rangeKey,
          item[rangeKey],
          schemaObject[rangeKey])
      }
    };
  }

  getDynamoDbTypeFromSchemaAttribute(schemaAttributeDefinition: any): string {
    switch (schemaAttributeDefinition.type.name) {
      case 'Number':
      case 'Date':
        return 'N';

      default:
        return 'S';
    }
  }

  parseDynamoDbTypeFromSchemaAttribute(fieldName: string, value: string, schemaAttributeDefinition: any): any {
    switch (schemaAttributeDefinition.type.name) {
      case 'Number':
        return value.valueOf().toString();

      case 'String':
        return value;

      case 'Date':
        return new Date(value).valueOf().toString();

      default:
        return value;
    }
  }

  async validate(schema, optData, options = { direction: ValidationDirection.SchemaToData }): Promise<any> {
    let data                      = Object.assign({}, optData);
    let errors: ValidationError[] = await this.validateAttributes(schema, data, options);

    if (errors.length > 0) {
      throw new ValidationErrors(errors);
    }

    return data;
  }

  async validateAttributes(schema: any,
                           data: any,
                           options: { direction: ValidationDirection },
                           sourcePrefix = '',
                           level        = 0): Promise<ValidationError[]> {

    let errors: ValidationError[] = [];

    let attributesToIterateOn = (options.direction === ValidationDirection.DataToSchema)
                                ? Object.keys(data)
                                : Object.keys(schema);

    // Catch early the unknown attributes.
    Object
      .keys(data)
      .forEach((sentAttribute) => {
        if (!(sentAttribute in schema)) {
          errors.push(new ValidationError(`${sourcePrefix}${(sourcePrefix) ? '/' : ''}${sentAttribute}`,
            ErrorMessages.UnknownAttribute.replace('{attribute}', sentAttribute)));
          return;
        }
      });

    // Return early for unknown attributes.
    if (errors.length > 0) {
      return errors;
    }

    for (let attribute of attributesToIterateOn) {
      const attributeSchema = schema[attribute];
      const source          = `${sourcePrefix}${(sourcePrefix) ? '/' : ''}${attribute}`;

      // Despite being blacklisted already, due a "feature" or bug, `id` is not getting filtered from the sent attributes.
      // See: https://github.com/danivek/json-api-serializer/issues/69
      // Only for the first level, since makes sense to have it on lists and maps.
      if (level === 0 && attribute === 'id') {
        continue;
      }
      // Should trigger only when `SchemaToData` (create).
      if (!(attribute in data) && attributeSchema.required) {
        errors.push(new ValidationError(source, ErrorMessages.MissingRequiredAttribute.replace('{attribute}', attribute)));
        continue;
      }
      // Should trigger only when `DataToSchema` (update).
      if (options.direction === ValidationDirection.DataToSchema
          && attributeSchema.readonly) {

        errors.push(new ValidationError(source, ErrorMessages.AttributeIsReadOnly.replace('{attribute}', attribute)));
      }
      // Should trigger only when `SchemaToData` (create).
      if ((!(attribute in data) || attributeSchema.enforceDefault)
          && ORM_SCHEMA_DEFAULT_PROPERTY_EXCLUSION_ATTRIBUTES_LIST.indexOf(attribute) === -1
          && attributeSchema.hasOwnProperty('default')) {

        try {
          data[attribute] = (typeof attributeSchema.default === 'function')
                            ? await attributeSchema.default.bind(this)(this)
                            : attributeSchema.default;
        }
        catch (e) {
          errors.push(new ValidationError(source, ErrorMessages.FailedDefaultDefinitionExecution.replace('{attribute}', attribute)));
          continue;
        }
      }
      // Should trigger only when `SchemaToData` (create).
      // Needed to re-check after default values were set.
      if (!(attribute in data)) {
        continue;
      }
      // Should trigger only when `DataToSchema` (update).
      // Only for the first level, since makes sense to have them on lists and maps.
      if (level === 0 && ['createdAt', 'updatedAt'].indexOf(attribute) !== -1) {
        errors.push(new ValidationError(source, ErrorMessages.TimestampsAttributesCannotBeDirectlyModified));
        continue;
      }

      const schemaType: any = attributeSchema.type;

      if (typeof schemaType === 'function'
          && typeof data[attribute] !== schemaType.name.toLowerCase()) {

        errors.push(new ValidationError(source, ErrorMessages.SchemaTypesMismatch
                                                             .replace('{attribute}', attribute)
                                                             .replace('{type}', schemaType.name)));

        continue;
      }
      if (schemaType === 'map') {
        if (!isPlainObject(data[attribute])) {
          errors.push(new ValidationError(source, ErrorMessages.SchemaTypesMismatch
                                                               .replace('{attribute}', attribute)
                                                               .replace('{type}', schemaType)));

          continue;
        }

        // Check for map children schema.
        const nestedErrors: ValidationError[] = await this.validateAttributes(attributeSchema.map,
          data[attribute],
          options,
          source,
          level + 1);

        errors = errors.concat(nestedErrors);
      }
      if (schemaType === 'list') {
        if (!Array.isArray(data[attribute])) {
          errors.push(new ValidationError(source, ErrorMessages.SchemaTypesMismatch
                                                               .replace('{attribute}', attribute)
                                                               .replace('{type}', schemaType)));

          continue;
        }

        // Check for list children schema.
        let filledListSchemaArr = new Array(data[attribute].length).fill(attributeSchema.list[0]);

        const nestedErrors: ValidationError[] = await this.validateAttributes({ ...filledListSchemaArr },
          data[attribute],
          options,
          source,
          level + 1);

        errors = errors.concat(nestedErrors);
      }

      // Check for `enums`.
      if (attributeSchema.enum
          && attributeSchema.enum.indexOf(data[attribute].toString()) === -1) {

        errors.push(new ValidationError(source, ErrorMessages.FailedEnumValidation
                                                             .replace('{attribute}', attribute)
                                                             .replace('{enumValues}', attributeSchema.enum.toString())
                                                             .replace('{value}', data[attribute])));

        continue;
      }

      if (!('validations' in attributeSchema)
          || Object.keys(attributeSchema.validations).length <= 0) {

        continue;
      }

      for (let validationKey in attributeSchema.validations) {
        if (!attributeSchema.validations.hasOwnProperty(validationKey)) {
          // To not iterate on unexpected (custom or inherited) members.
          continue;
        }

        let validationValue = attributeSchema.validations[validationKey];
        let args;

        if (typeof validationValue === 'function') {
          // A custom validator.
          try {
            let isValid = await validationValue.bind(data)(data[attribute], this);

            if (typeof isValid === 'string') {
              errors.push(new ValidationError(source, isValid));
              continue;
            }
            else if (typeof isValid === 'boolean' && !isValid) {
              errors.push(new ValidationError(source, ErrorMessages.FailedCustomValidation
                                                                   .replace('{attribute}', attribute)
                                                                   .replace('{validation}', validationKey)));
            }
            else {
              continue;
            }
          }
          catch (e) {
            errors.push(new ValidationError(source, e.message));
            continue;
          }
          continue;
        }
        else if (Array.isArray(validationValue)) {
          args = validationValue;
        }
        else if (typeof validationValue === 'boolean') {
          args = [];
        }

        // Last in the algorithm to allow support for custom function validators.
        if (!(validationKey in validator)) {
          errors.push(new ValidationError(source, ErrorMessages.UnknownValidation.replace('{validation}', validationKey)));
          continue;
        }

        if (!validator[validationKey].apply(null, [data[attribute], ...args])) {
          errors.push(new ValidationError(source, ErrorMessages.FailedValidation
                                                               .replace('{attribute}', attribute)
                                                               .replace('{validation}', validationKey)
                                                               .replace('{args}', args.join(','))));
        }
      }
    }

    return errors;
  }
}
