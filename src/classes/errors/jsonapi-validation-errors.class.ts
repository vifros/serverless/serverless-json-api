import { statusCodes }     from '../../constants/jsonapi';
import { IJsonApiErrors }  from '../../interfaces/jsonapi.interfaces';
import { JsonApiError }    from './jsonapi-error.class';
import { ValidationError } from './validation-error.class';

export class JsonApiValidationErrors extends Error {
  name                   = 'JsonApiValidationErrors';
  errors: JsonApiError[] = [];

  constructor(errors: ValidationError[] = []) {
    // 'Error' breaks prototype chain here.
    super('JsonApiValidationErrors');

    // Fixing the break.
    Object.setPrototypeOf(this, JsonApiValidationErrors.prototype);

    this.errors = errors.map((error: ValidationError) => {
      return new JsonApiError(statusCodes.unprocessableEntity.id,
        error.title,
        null,
        {
          pointer: `/data/attributes/${error.source}`
        });
    });
  }

  toJSON(): IJsonApiErrors {
    let objectToStringify = {};

    [
      'errors'
    ].forEach((field) => {
      if (this[field]) {
        objectToStringify[field] = this[field];
      }
    });

    return objectToStringify;
  }
}
