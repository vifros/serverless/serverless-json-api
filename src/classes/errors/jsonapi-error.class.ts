import { IGenericMap } from '../../interfaces/generic.interfaces';

import {
  IJsonApiError,
  IJsonApiErrorLinks,
  IJsonApiErrorSource
} from '../../interfaces/jsonapi.interfaces';

export class JsonApiError extends Error {
  name = 'JsonApiError';

  constructor(public status: number,
              public title: string,
              public detail?: string,
              public source?: IJsonApiErrorSource,
              public code?: string,
              public id?: string,
              public links?: IJsonApiErrorLinks,
              public meta?: IGenericMap) {

    // 'Error' breaks prototype chain here.
    super('JsonApiError');

    // Fixing the break.
    Object.setPrototypeOf(this, JsonApiError.prototype);
  }

  toJSON(): IJsonApiError {
    let objectToStringify = {};

    [
      'status',
      'title',
      'id',
      'links',
      'code',
      'detail',
      'source',
      'meta'
    ].forEach((field) => {
      if (this[field]) {
        objectToStringify[field] = this[field];
      }
    });

    return objectToStringify;
  }
}
