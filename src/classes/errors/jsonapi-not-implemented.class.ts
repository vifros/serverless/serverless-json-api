import {
  ErrorMessages,
  ErrorTitles
} from '../../constants/errors';

import { JsonApi }      from '../jsonapi.class';
import { JsonApiError } from './jsonapi-error.class';

export class JsonApiNotImplementedError extends JsonApiError {
  name = 'JsonApiNotImplementedError';

  constructor() {
    // 'Error' breaks prototype chain here.
    super(
      JsonApi.statusCodes.notImplemented.id,
      ErrorTitles.NotImplemented,
      ErrorMessages.NotImplemented);

    // Fixing the break.
    Object.setPrototypeOf(this, JsonApiNotImplementedError.prototype);
  }
}
