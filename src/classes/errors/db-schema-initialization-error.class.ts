export class DbSchemaInitializationError extends Error {
  name = 'DbSchemaInitializationError';

  constructor(public source: string,
              public message: string) {

    // 'Error' breaks prototype chain here.
    super(message);

    // Fixing the break.
    Object.setPrototypeOf(this, DbSchemaInitializationError.prototype);
  }

  toJSON(): any {
    let objectToStringify = {};

    [
      'source',
      'message'
    ].forEach((field) => {
      if (this[field]) {
        objectToStringify[field] = this[field];
      }
    });

    return objectToStringify;
  }
}
