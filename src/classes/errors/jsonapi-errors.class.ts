import { IJsonApiErrors } from '../../interfaces/jsonapi.interfaces';
import { JsonApiError }   from './jsonapi-error.class';

export class JsonApiErrors extends Error {
  name = 'JsonApiErrors';

  constructor(public errors: JsonApiError[],
              public status?: number) {

    // 'Error' breaks prototype chain here.
    super('JsonApiErrors');

    // Fixing the break.
    Object.setPrototypeOf(this, JsonApiErrors.prototype);
  }

  toJSON(): IJsonApiErrors {
    let objectToStringify = {};

    [
      'errors',
      'status'
    ].forEach((field) => {
      if (this[field]) {
        objectToStringify[field] = this[field];
      }
    });

    return objectToStringify;
  }
}
