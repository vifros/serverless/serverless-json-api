import { JsonApi }      from '../jsonapi.class';
import { JsonApiError } from './jsonapi-error.class';

export class JsonApiNotFoundError extends JsonApiError {
  name = 'JsonApiNotFoundError';

  constructor(source?: any) {
    // 'Error' breaks prototype chain here.
    super(
      JsonApi.statusCodes.notFound.id,
      JsonApi.statusCodes.notFound.text,
      null,
      source);

    // Fixing the break.
    Object.setPrototypeOf(this, JsonApiNotFoundError.prototype);
  }
}
