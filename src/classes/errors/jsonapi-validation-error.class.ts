import { JsonApi }      from '../jsonapi.class';
import { JsonApiError } from './jsonapi-error.class';

export class JsonApiValidationError extends JsonApiError {
  name = 'JsonApiValidationError';

  constructor(title: string, detail: string) {
    // 'Error' breaks prototype chain here.
    super(
      JsonApi.statusCodes.unprocessableEntity.id,
      title,
      detail);

    // Fixing the break.
    Object.setPrototypeOf(this, JsonApiValidationError.prototype);
  }
}
