export class ValidationError extends Error {
  name = 'ValidationError';

  constructor(public source: string,
              public title: string) {

    // 'Error' breaks prototype chain here.
    super('ValidationError');

    // Fixing the break.
    Object.setPrototypeOf(this, ValidationError.prototype);
  }

  toJSON(): any {
    let objectToStringify = {};

    [
      'source',
      'title'
    ].forEach((field) => {
      if (this[field]) {
        objectToStringify[field] = this[field];
      }
    });

    return objectToStringify;
  }
}
