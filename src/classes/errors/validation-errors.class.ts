import { ValidationError } from './validation-error.class';

export class ValidationErrors extends Error {
  name = 'ValidationErrors';

  constructor(public errors: ValidationError[]) {
    // 'Error' breaks prototype chain here.
    super('ValidationErrors');

    // Fixing the break.
    Object.setPrototypeOf(this, ValidationErrors.prototype);
  }

  toJSON(): any {
    let objectToStringify = {};

    [
      'errors'
    ].forEach((field) => {
      if (this[field]) {
        objectToStringify[field] = this[field];
      }
    });

    return objectToStringify;
  }
}
