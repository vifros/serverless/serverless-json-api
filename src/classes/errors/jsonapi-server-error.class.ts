import { JsonApi }      from '../jsonapi.class';
import { JsonApiError } from './jsonapi-error.class';

export class JsonApiServerError extends JsonApiError {
  name = 'JsonApiServerError';

  constructor(public error?: Error) {
    // 'Error' breaks prototype chain here.
    super(
      JsonApi.statusCodes.internalServerError.id,
      JsonApi.statusCodes.internalServerError.text);

    // Fixing the break.
    Object.setPrototypeOf(this, JsonApiServerError.prototype);
  }
}
