const serverless = require('serverless-http');

import {
  APIGatewayEvent,
  APIGatewayProxyHandler,
  Context
} from 'aws-lambda';

import * as bodyParser           from 'body-parser';
import * as express              from 'express';
import * as asyncHandler         from 'express-async-handler';
import { Express, NextFunction } from 'express';

import { IRoute }          from 'express-serve-static-core';
import * as helmet         from 'helmet';
import * as methodOverride from 'method-override';

import { HttpMethods }   from '..';
import { EndpointTypes } from '../constants/jsonapi';

import {
  IAppRequest,
  IAppResponse,
  IAppRouteConfig,
  IScope
} from '../interfaces/app.interfaces';

import { IDbModelLoaderMap } from '../interfaces/db.interfaces';
import { BaseHandler }       from './base-handler.class';
import { Db }                from './db.class';
import { JsonApi }           from './jsonapi.class';

export class ServerlessJsonApi {
  private readonly providers: Map<string, typeof BaseHandler>;
  private routes: Set<IAppRouteConfig>;

  readonly app: Express = express();
  readonly db: Db       = new Db();

  readonly handlers: Map<string, BaseHandler> = new Map();

  constructor(providers: Map<string, typeof BaseHandler> = new Map(),
              models: Set<IDbModelLoaderMap>             = new Set()) {

    this.providers = providers;
    this.initHandlers();
    this.initCoreMiddleware();
    this.db.registerModels(models);
  }

  private initHandlers() {
    if (this.providers.size <= 0) {
      return;
    }

    this.providers.forEach((Handler: typeof BaseHandler, id: string) => {
      this.handlers.set(id, new Handler());
    });
  }

  private setHandlersScope(scope: IScope) {
    if (this.handlers.size <= 0) {
      return;
    }

    this.handlers.forEach((handler: BaseHandler) => {
      handler.setScope(scope);
    });
  }

  private initCoreMiddleware() {
    this.app.use(bodyParser.json({
      strict: false,
      type  : JsonApi.mediaType
    }));

    this.app.use(helmet());
    this.app.use(methodOverride());

    JsonApi.registerMiddleware(this.app);
  }

  private extractParamsInOrderFromRoute(route: string, reqParams: any): string[] {
    let regex  = RegExp(/(\:\w+)/, 'g');
    let params = [];
    let buffer;

    while ((buffer = regex.exec(route)) !== null) {
      params.push(reqParams[buffer[0].slice(1)]);
    }

    return params;
  }

  get mainHandler(): APIGatewayProxyHandler | any {
    return serverless(this.app, {
      request: (request: IAppRequest,
                event: APIGatewayEvent,
                context: Context) => {

        const scope: IScope = {
          req              : request,
          serverlessJsonApi: this,
          apiGateway       : {
            event  : event,
            context: context
          }
        };

        this.setHandlersScope(scope);
        request.scope = scope;
      }
    });
  };

  getHandler(id: string): BaseHandler {
    return this.handlers.get(id);
  }

  registerRoutes(routes: Set<IAppRouteConfig> = new Set()) {
    this.routes = routes;

    if (this.routes.size <= 0) {
      return;
    }

    this.routes.forEach((appRouteConfig: IAppRouteConfig) => {
      const route: IRoute = this.app.route(appRouteConfig.route);

      route[appRouteConfig.method](asyncHandler(async (req: IAppRequest, res: IAppResponse, next: NextFunction) => {
        res.set('Access-Control-Allow-Origin', '*');
        res.set('Access-Control-Allow-Credentials', 'true');

        let orderedParams: any[] = this.extractParamsInOrderFromRoute(appRouteConfig.route, req.params);

        JsonApi.validateRequest(appRouteConfig, req, orderedParams, this.db.modelLoaders.get(appRouteConfig.relatedModelId));

        if (req.method.toLowerCase() === HttpMethods.POST
            || req.method.toLowerCase() === HttpMethods.PATCH) {

          const data = this.db.serializer.deserialize(appRouteConfig.relatedModelId, req.body);
          orderedParams.push(data);
        }

        if (Object.keys(req.query).length > 0) {
          orderedParams.push(req.query);
        }

        orderedParams.push({
          isFirstInChain: true
        });

        // TODO @diosney: If there are missing params then throw a JSON-API error.
        // TODO @diosney: See how fix the `this` reference here in the call instead of the route registration.
        // TODO @diosney: Move all this serialization into JsonApi static method to clear up space here?
        let dataToReturn: any = await appRouteConfig.handler(...orderedParams);

        if (appRouteConfig.endpointType === EndpointTypes.Collection) {
          if (req.method.toLowerCase() === HttpMethods.POST) {
            let serializedData = this.db.serializer.serialize(appRouteConfig.relatedModelId, dataToReturn);

            // TODO @diosney: Later set it as an absolute URI, not a related one like now.
            res.set('Location', serializedData.data.links.self);

            return res.status(JsonApi.statusCodes.created.id)
                      .json(serializedData);
          }
          if (req.method.toLowerCase() === HttpMethods.GET) {
            let serializedData = this.db.serializer.serialize(appRouteConfig.relatedModelId, dataToReturn);
            return res.status(JsonApi.statusCodes.ok.id)
                      .json(serializedData);
          }
        }

        if (appRouteConfig.endpointType === EndpointTypes.Resource) {
          if (req.method.toLowerCase() === HttpMethods.GET
              || req.method.toLowerCase() === HttpMethods.PATCH) {

            let serializedData = this.db.serializer.serialize(appRouteConfig.relatedModelId, dataToReturn);
            return res.status(JsonApi.statusCodes.ok.id)
                      .json(serializedData);
          }
          if (req.method.toLowerCase() === HttpMethods.DELETE) {
            return res.status(JsonApi.statusCodes.noContent.id)
                      .end();
          }
        }
      }));

      JsonApi.registerErrorMiddleware(this.app);
    });
  }
}
