import { Request } from 'express';

import { IDbModelLoaderMap } from '../interfaces/db.interfaces';
import { IScope }            from '../interfaces/app.interfaces';
import { IGenericMap }       from '../interfaces/generic.interfaces';
import { Db }                from './db.class';

export class BaseHandler {
  scope: IScope;

  constructor() {
  }

  get db(): Db {
    return this.scope.serverlessJsonApi.db;
  }

  get req(): Request {
    return this.scope.req;
  }

  get params(): IGenericMap {
    return this.scope.req.params;
  }

  get query(): IGenericMap {
    return this.scope.req.query;
  }

  get body(): IGenericMap {
    return this.scope.req.body;
  }

  get serializer(): any {
    return this.db.serializer;
  }

  setScope(scope: IScope) {
    this.scope = scope;
  }

  getModelById(id: string): any {
    return this.db.models.get(id);
  }

  getModelByLoader(modelLoader: IDbModelLoaderMap): any {
    return this.db.models.get(modelLoader.id);
  }

  getHandlerById(id: string): BaseHandler {
    return this.scope.serverlessJsonApi.handlers.get(id);
  }
}
