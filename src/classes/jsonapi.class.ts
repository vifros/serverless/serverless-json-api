import {
  Express,
  NextFunction,
  Request,
  Response
} from 'express';

import mergeWith     from 'lodash.mergewith';
import isPlainObject from 'lodash.isplainobject';

import {
  ErrorMessages,
  ErrorTitles,
  ErrorTypes
} from '../constants/errors';

import {
  EndpointTypes,
  mediaType as JsonApiMediaType,
  statusCodes as JsonApiStatusCodes
} from '../constants/jsonapi';

import { IDbModelLoaderMap }             from '../interfaces/db.interfaces';
import { JSONAPI_EXTRA_SYMBOL }          from '../constants/symbols';
import { HttpMethods }                   from '../interfaces/http.interfaces';
import { IAppRouteConfig }               from '../interfaces/app.interfaces';
import { Stages }                        from '../constants/env';
import { IGenericIdTextMap }             from '../interfaces/generic.interfaces';
import { IJsonApiErrorResponse }         from '../interfaces/jsonapi.interfaces';
import { JsonApiError }                  from './errors/jsonapi-error.class';
import { JsonApiErrors }                 from './errors/jsonapi-errors.class';
import { JsonApiNotFoundError }          from './errors/jsonapi-not-found-error.class';
import { JsonApiServerError }            from './errors/jsonapi-server-error.class';
import { JsonApiValidationErrors }       from './errors/jsonapi-validation-errors.class';
import { ValidationErrors }              from './errors/validation-errors.class';
import { getExtras as JsonApiGetExtras } from './jsonapi-cyclic.class';

export namespace JsonApi {
  export const mediaType = JsonApiMediaType;

  export const statusCodes: IGenericIdTextMap<number> = JsonApiStatusCodes;

  // Better the larger it is, but not to large :).
  export const pageSizeOptimizationForLists = 25;
  export const maximumPageSize              = 25;
  export const defaultPageSize              = 10;

  export const defaultSort = 'createdAt';

  const filterOperators = new Set([
    '$not',
    '$null',
    '$eq',
    '$lt',
    '$le',
    '$ge',
    '$gt',
    '$beginsWith',
    '$between',
    '$contains',
    '$in'
  ]);

  export function registerMiddleware(app: Express) {
    app.use((req: Request, res: Response, next: NextFunction) => {
      // TODO @diosney: Later check how remove hardcoded charset that Express.js append like `; charset=utf-8`.
      res.type(mediaType);

      if (!req.is(mediaType) && [
        HttpMethods.POST,
        HttpMethods.PATCH,
        HttpMethods.PUT
      ].indexOf(req.method.toLowerCase() as HttpMethods) !== -1) {

        let jsonApiError: JsonApiError = new JsonApiError(
          statusCodes.unsupportedMediaType.id,
          statusCodes.unsupportedMediaType.text,
          `The only supported media type is '${mediaType}'.`);

        res.status(statusCodes.unsupportedMediaType.id)
           .json(serializeError(jsonApiError));

        return;
      }
      if (req.get('Content-Type') && req.get('Content-Type').search(/;/) !== -1) {
        let jsonApiError: JsonApiError = new JsonApiError(
          statusCodes.notAcceptable.id,
          statusCodes.notAcceptable.text,
          statusCodes.notAcceptable.detail);

        res.status(statusCodes.notAcceptable.id)
           .json(serializeError(jsonApiError));

        return;
      }

      next();
    });
  }

  function processJsonApiCatchAllError(res: Response, error: any) {
    if ((error as any).error) {
      let newError = (error as any).error;

      if (newError.errors) {
        return res.status(newError.status || statusCodes.internalServerError.id)
                  .json(serializeError(newError.errors));
      }
      return res.status(newError.status || statusCodes.internalServerError.id)
                .json(serializeError(newError));
    }
    if (process.env.SERVICE_STAGE === Stages.Development
        || process.env.SERVICE_STAGE === Stages.Local) {

      return res.status((error as JsonApiError).status || statusCodes.internalServerError.id)
                .json(serializeError(new JsonApiError(statusCodes.internalServerError.id,
                  error.name,
                  error.message)));
    }

    return res.status(statusCodes.internalServerError.id)
              .json(serializeError(new JsonApiServerError()));
  }

  export function registerErrorMiddleware(app: Express) {
    app.use((error: Error | JsonApiErrors, req: Request, res: Response, next: NextFunction) => {
      switch (error.name) {
        case ErrorTypes.ConditionalCheckFailedException: {
          return res.status(statusCodes.notFound.id)
                    .json(serializeError(new JsonApiNotFoundError()));
        }

        case ErrorTypes.JsonApiErrors:
        case ErrorTypes.JsonApiValidationErrors: {
          return res.status(statusCodes.unprocessableEntity.id)
                    .json(serializeError((error as JsonApiErrors).errors));
        }

        case ErrorTypes.ValidationErrors: {
          return res.status(statusCodes.unprocessableEntity.id)
                    .json(serializeError((new JsonApiValidationErrors((error as ValidationErrors).errors) as JsonApiErrors).errors));
        }

        case ErrorTypes.JsonApiError:
        case ErrorTypes.JsonApiValidationError:
        case ErrorTypes.JsonApiNotFoundError:
        case ErrorTypes.JsonApiNotImplementedError: {
          return res.status((error as JsonApiError).status || statusCodes.badRequest.id)
                    .json(serializeError(error as JsonApiError));
        }

        case ErrorTypes.JsonApiServerError: {
          if ((error as JsonApiServerError).error
              && (process.env.SERVICE_STAGE === Stages.Development
                  || process.env.SERVICE_STAGE === Stages.Local)) {

            return res.status((error as JsonApiError).status || statusCodes.internalServerError.id)
                      .json(serializeError(new JsonApiError(statusCodes.internalServerError.id,
                        (error as JsonApiServerError).error.name,
                        (error as JsonApiServerError).error.message)));
          }

          return res.status((error as JsonApiError).status || statusCodes.internalServerError.id)
                    .json(serializeError(error as JsonApiError));
        }

        default: {
          return processJsonApiCatchAllError(res, error);
        }
      }
    });
  }

  export function serializeError(error: JsonApiError | JsonApiError[]): IJsonApiErrorResponse {
    return {
      errors: (Array.isArray(error))
              ? error
              : [error]
    };
  }

  export function validateRequest(appRouteConfig: IAppRouteConfig,
                                  req: Request,
                                  orderedParams: string[],
                                  modelLoader: IDbModelLoaderMap): any {

    let errors: JsonApiError[] = [];

    if (req.method.toLowerCase() === HttpMethods.POST
        || req.method.toLowerCase() === HttpMethods.PATCH) {

      if (!req.body.data) {
        throw new JsonApiError(
          statusCodes.unprocessableEntity.id,
          ErrorTitles.InvalidRequest,
          ErrorMessages.MissingDataMemberAtDocument,
          {
            pointer: ''
          });
      }
      if (!req.body.data.type) {
        throw new JsonApiError(
          statusCodes.unprocessableEntity.id,
          ErrorTitles.InvalidRequest,
          ErrorMessages.MissingTypeMemberAtData,
          {
            pointer: '/data/type'
          });
      }
      if (req.body.data.type !== appRouteConfig.relatedModelId) {
        throw new JsonApiError(
          statusCodes.conflict.id,
          statusCodes.conflict.text,
          ErrorMessages.InvalidType,
          {
            pointer: '/data/type'
          });
      }
    }
    if (req.method.toLowerCase() === HttpMethods.POST) {
      let allowClientGeneratedId: boolean = false;

      if (modelLoader.metaOptions
          && modelLoader.metaOptions.hasOwnProperty('allowClientGeneratedIds')) {

        allowClientGeneratedId = modelLoader.metaOptions.allowClientGeneratedIds;
      }

      if (req.body.data.id && !allowClientGeneratedId) {
        throw new JsonApiError(
          statusCodes.forbidden.id,
          statusCodes.forbidden.text,
          ErrorMessages.PresentIdMemberAtData,
          {
            pointer: '/data/id'
          });
      }
    }
    if (req.method.toLowerCase() === HttpMethods.PATCH) {
      if (!req.body.data.id) {
        throw new JsonApiError(
          statusCodes.unprocessableEntity.id,
          ErrorTitles.InvalidRequest,
          ErrorMessages.MissingIdMemberAtData,
          {
            pointer: '/data/id'
          });
      }
      if (req.body.data.id !== orderedParams[orderedParams.length - 1]) {
        throw new JsonApiError(
          statusCodes.conflict.id,
          ErrorTitles.InvalidRequest,
          ErrorMessages.IdMismatch,
          {
            pointer: '/data/id'
          });
      }
      if (!req.body.data.attributes) {
        throw new JsonApiError(
          statusCodes.unprocessableEntity.id,
          ErrorTitles.InvalidRequest,
          ErrorMessages.MissingAttributesMemberAtData,
          {
            pointer: '/data/attributes'
          });
      }
      if (Object.keys(req.body.data.attributes).length <= 0) {
        throw new JsonApiError(
          statusCodes.unprocessableEntity.id,
          ErrorTitles.InvalidRequest,
          ErrorMessages.NotAllowedEmptyAttributesMember,
          {
            pointer: '/data/attributes'
          });
      }
      if (req.body.data.attributes.id) {
        throw new JsonApiError(
          statusCodes.unprocessableEntity.id,
          ErrorTitles.InvalidAttribute,
          ErrorMessages.IdAttributeNotAllowedForUpdate,
          {
            pointer: '/data/attributes/id'
          });
      }
    }
    if (req.method.toLowerCase() === HttpMethods.GET
        && appRouteConfig.endpointType === EndpointTypes.Collection) {

      if (req.query) {
        errors.push(...validateJsonApiQuery(req.query));
      }
    }

    if (errors.length > 0) {
      throw new JsonApiErrors(errors, statusCodes.badRequest.id);
    }
  }

  function validateJsonApiQuery(query: any): any[] {
    let errors: any = [];

    if (query) {
      if (query.sort) {
        // Multiple sorting fields are not supported.
        if (query.sort.search(',') !== -1) {
          errors.push(new JsonApiError(
            statusCodes.badRequest.id,
            ErrorTitles.InvalidParameter,
            ErrorMessages.MultipleSortParametersNotSupported,
            {
              parameter: 'sort'
            }));
        }
      }
      if (query.page) {
        let pageSize = (query.page.size)
                       ? parseInt(query.page.size)
                       : defaultPageSize;

        if (isNaN(pageSize)) {
          errors.push(new JsonApiError(
            statusCodes.badRequest.id,
            ErrorTitles.InvalidParameter,
            ErrorMessages.InvalidPageSize.replace('{got}', query.page.size),
            {
              parameter: 'page[size]'
            },
            null,
            null));
        }
        if (query.page.before && query.page.after) {
          errors.push(new JsonApiError(
            statusCodes.badRequest.id,
            ErrorTitles.InvalidParameter,
            ErrorMessages.RangePaginationNotSupported,
            {
              parameter: 'page'
            },
            null,
            null,
            {
              type: 'https://jsonapi.org/profiles/ethanresnick/cursor-pagination/range-pagination-not-supported'
            }));
        }
        if (pageSize <= 0) {
          errors.push(new JsonApiError(
            statusCodes.badRequest.id,
            ErrorTitles.InvalidParameter,
            ErrorMessages.InvalidPageSize.replace('{got}', query.page.size),
            {
              parameter: 'page[size]'
            }));
        }
        if (pageSize > maximumPageSize) {
          errors.push(new JsonApiError(
            statusCodes.badRequest.id,
            ErrorTitles.MaximumPageSizeExceeded,
            ErrorMessages.MaximumPageSizeExceeded
                         .replace('{requestedPageSize}', query.page.size)
                         .replace('{maximumPageSize}', maximumPageSize.toString()),
            {
              parameter: 'page[size]'
            },
            null,
            null,
            {
              type: 'https://jsonapi.org/profiles/ethanresnick/cursor-pagination/max-size-exceeded'
            }, {
              page: {
                maxSize: maximumPageSize
              }
            }));
        }
      }
    }

    return errors;
  }

  // Important note: the `.limit` will always be set to `pageSizeOptimizationForLists`, so the querying is optimized,
  // so no matter what `query.page.size` is passed as parameter, it will be reset to `pageSizeOptimizationForLists`.
  export function buildOrmQueryObjectFromReq(model: any, type: string, optQuery: any, schemaObject: any): any {
    let reqQuery               = Object.assign({}, optQuery);
    let errors: JsonApiError[] = [];

    errors.push(...validateJsonApiQuery(reqQuery));

    // Sorting.
    // Query example: `sort: '-created'`.
    // Multiple sorting fields not allowed: bad: `sort: '-created,title'`.
    // Important: It has to be the range key!
    if (!('sort' in reqQuery)) {
      reqQuery.sort = defaultSort;
    }

    let rangeKey: string = (reqQuery.sort[0] === '+' || reqQuery.sort[0] === '-')
                           ? reqQuery.sort.substring(1).trim()
                           : reqQuery.sort.trim();

    let uppercasedRangeKey = rangeKey[0].toUpperCase() + rangeKey.slice(1);

    let query: any = model
      .query('type')
      .using(`type${uppercasedRangeKey}GlobalIndex`) // TODO @diosney: Later get this directly from the schema?
      .eq(type);

    // Filtering.
    // Query example: `?filter[fieldName]=fieldPartialValue`.
    // Query example: `?filter[fieldName]=value1&filter[fieldName]=value2`.
    // For more information about valid operators, see https://dynamoosejs.com/api/query.
    // Check too http://sequelize.readthedocs.org/en/latest/docs/querying.
    if ('filter' in reqQuery) {
      buildQueryFromFilter(query, reqQuery.filter, schemaObject, rangeKey);
    }

    switch (reqQuery.sort[0]) {
      case '+':
        query.ascending();
        break;

      case '-':
        query.descending();
        break;

      default:
        query.ascending();
        break;
    }

    // Pagination.
    // Query example: `page[size]=100&page[after]=abcd`.
    if (!('page' in reqQuery)) {
      reqQuery.page = {};
    }
    if (!('size' in reqQuery.page)) {
      reqQuery.page.size = defaultPageSize;
    }
    else {
      let parsedPageSize: number = parseInt(reqQuery.page.size);
      if (isNaN(parsedPageSize)) {
        reqQuery.page.size = defaultPageSize;
      }
      else {
        reqQuery.page.size = parsedPageSize;
      }
    }

    // Intended, discarded `page.size` in favor of `pageSizeOptimizationForLists` for consistent optimized search queries.
    query.limit(pageSizeOptimizationForLists);

    if (reqQuery.page.after
        && reqQuery.page.after !== 'null') { // Due the base64 encoding.

      // TODO @diosney: Validate this.
      query.startAt(JSON.parse(Buffer.from(reqQuery.page.after, 'base64').toString()));
    }

    // Sparse fieldset.
    // Query example: `fields[articles]=title,body&fields[people]=name`.
    // In this case, only search for the `type` passed down.
    if ('fields' in reqQuery && reqQuery.fields[type]) {
      // Include the range key since it is always needed.
      let fieldsToReturn: string[] = reqQuery.fields[type]
        .replace(/\s/g, '')
        .split(',');

      if (fieldsToReturn.indexOf(rangeKey) === -1) {
        fieldsToReturn.push(rangeKey);
      }
      if (fieldsToReturn.indexOf('id') === -1) {
        fieldsToReturn.push('id');
      }

      query.attributes(fieldsToReturn);
    }

    if (errors.length > 0) {
      throw new JsonApiErrors(errors);
    }

    return {
      ormQuery: query,
      reqQuery,
      rangeKey
    };
  }

  function buildQueryFromFilter(query: any, optFilter: any, schemaObject: any, rangeKey: string) {
    let filter           = optFilter;
    let keys             = Object.keys(optFilter);
    let filterPathPrefix = 'filter';

    let hasAndOperator = keys.some((key) => {
      return key === '$and';
    });

    let hasOrOperator = keys.some((key) => {
      return key === '$or';
    });

    let hasTopLevelFields = keys.some((key) => {
      return key !== '$and' && key !== '$or';
    });

    if (hasAndOperator && hasOrOperator) {
      throw new JsonApiError(
        statusCodes.unprocessableEntity.id,
        ErrorTitles.InvalidParameter,
        ErrorMessages.BothAndOrConditionalFiltersCannotBePresent,
        {
          parameter: 'filter'
        });
    }
    if (hasTopLevelFields && (hasAndOperator || hasOrOperator)) {
      throw new JsonApiError(
        statusCodes.unprocessableEntity.id,
        ErrorTitles.InvalidParameter,
        ErrorMessages.BothFieldAndOrConditionalFiltersCannotBePresent,
        {
          parameter: 'filter'
        });
    }
    if (keys.length <= 0
        || typeof filter === 'string'
        || typeof filter === 'number'
        || typeof filter === 'boolean') {

      throw new JsonApiError(
        statusCodes.unprocessableEntity.id,
        ErrorTitles.InvalidParameter,
        ErrorMessages.AtLeastOneConditionalFilterMustBePresent,
        {
          parameter: 'filter'
        });
    }

    if (hasAndOperator) {
      filter = optFilter.$and;
      filterPathPrefix += '[$and]';
    }
    if (hasOrOperator) {
      filter = optFilter.$or;
      filterPathPrefix += '[$or]';
    }
    if (hasAndOperator || hasOrOperator) {
      keys = Object.keys(filter);

      hasTopLevelFields = keys.some((key) => {
        return key !== '$and' && key !== '$or';
      });
    }

    // Process a top level filter with only fields.
    if (hasTopLevelFields) {
      if (keys.indexOf(rangeKey) !== -1) {
        // Reorder it to be first since `where` needs to be first than filter.
        let rangeKeyIndex = keys.findIndex((item) => {
          return item === rangeKey;
        });

        let removed = keys.splice(rangeKeyIndex, 1);
        keys.unshift(...removed);
      }

      keys.forEach((fieldName: string, index: number) => {
        if (typeof filter[fieldName] === 'string'
            || typeof filter[fieldName] === 'number'
            || typeof filter[fieldName] === 'boolean') {

          throw new JsonApiError(
            statusCodes.unprocessableEntity.id,
            ErrorTitles.InvalidParameter,
            ErrorMessages.NoConditionalFiltersWereFoundInField.replace('{field}', fieldName),
            {
              parameter: `${filterPathPrefix}[${fieldName}]`
            });
        }

        if (!schemaObject[fieldName]) {
          throw new JsonApiError(
            statusCodes.unprocessableEntity.id,
            ErrorTitles.InvalidParameter,
            ErrorMessages.UnknownFieldToFilterFor.replace('{field}', fieldName),
            {
              parameter: `${filterPathPrefix}[${fieldName}]`
            });
        }

        let operatorsReceived = Object.keys(filter[fieldName]);

        if (operatorsReceived.length > 1) {
          throw new JsonApiError(
            statusCodes.unprocessableEntity.id,
            ErrorTitles.InvalidParameter,
            ErrorMessages.MultipleConditionalFiltersForSameFieldIsNotSupported,
            {
              parameter: `${filterPathPrefix}[${fieldName}]`
            });
        }

        let operator       = operatorsReceived[0];
        let hasNotOperator = operator === '$not';

        if (hasNotOperator) {
          if (typeof filter[fieldName].$not === 'string'
              || typeof filter[fieldName].$not === 'number'
              || typeof filter[fieldName].$not === 'boolean') {

            throw new JsonApiError(
              statusCodes.unprocessableEntity.id,
              ErrorTitles.InvalidParameter,
              ErrorMessages.NoConditionalFiltersWereFoundInField.replace('{field}', fieldName),
              {
                parameter: `${filterPathPrefix}[${fieldName}][$not]`
              });
          }

          let notInnerOperators = Object.keys(filter[fieldName].$not);

          if (notInnerOperators.length > 1) {
            throw new JsonApiError(
              statusCodes.unprocessableEntity.id,
              ErrorTitles.InvalidParameter,
              ErrorMessages.MultipleConditionalFiltersForSameFieldIsNotSupported,
              {
                parameter: `${filterPathPrefix}[${fieldName}][$not]`
              });
          }

          operator = notInnerOperators[0];
        }

        if (!filterOperators.has(operator)) {
          throw new JsonApiError(
            statusCodes.unprocessableEntity.id,
            ErrorTitles.InvalidParameter,
            ErrorMessages.UnknownConditionalFilter.replace('{filter}', operator),
            {
              parameter: (hasNotOperator)
                         ? `${filterPathPrefix}[${fieldName}][$not][${operator}]`
                         : `${filterPathPrefix}[${fieldName}][${operator}]`
            });
        }

        let operatorValue = (hasNotOperator)
                            ? filter[fieldName].$not[operator]
                            : filter[fieldName][operator];

        switch (schemaObject[fieldName].type.name) {
          case 'Number':
            if (Array.isArray(operatorValue)) {
              operatorValue = operatorValue.map((item) => {
                return Number(item);
              });
            }
            else {
              operatorValue = [Number(operatorValue)];
            }

            break;

          case 'Date':
          case 'String':
          default:
            if (!Array.isArray(operatorValue)) {
              operatorValue = [operatorValue];
            }
            break;
        }

        if (fieldName === rangeKey) {
          query.where(rangeKey);

          if (hasNotOperator) {
            query.not()[operator.replace('$', '')](...operatorValue);
          }
          else {
            query[operator.replace('$', '')](...operatorValue);
          }
        }
        else {
          // index === 0 is when no `where` was added (no filter on rangeKey).
          // index === 1 is when a `where` was added (filter on rangeKey).
          //
          // On those cases, the `and` is not needed since it is already implicit.
          if (index >= 1 && !hasOrOperator) {
            query.and();
          }

          if (index > 0 && hasOrOperator) {
            query.or();
          }

          query.filter(fieldName);

          // This ugly repeated if-else is due `this` is lost if we cache the query.
          if (hasNotOperator) {
            if (operator === '$in') {
              query.not()[operator.replace('$', '')](operatorValue);
            }
            else {
              query.not()[operator.replace('$', '')](...operatorValue);
            }
          }
          else {
            if (operator === '$in') {
              query[operator.replace('$', '')](operatorValue);
            }
            else {
              query[operator.replace('$', '')](...operatorValue);
            }
          }
        }
      });
    }
  }

  /**
   * @deprecated
   *
   * @param object
   * @param property
   * @param value
   */
  export function defineExtraProperty(object: any, property: string, value: any) {
    Object.defineProperty(object, 'extra', {
      enumerable  : false,
      configurable: false,
      writable    : false,
      value       : {
        [property]: value
      }
    });
  }

  export function getExtras(object: any): any {
    return JsonApiGetExtras(object);
  }

  export function addExtras(object: any, values: any) {
    let objectSymbols: symbol[] = Object.getOwnPropertySymbols(object);

    if (objectSymbols.indexOf(JSONAPI_EXTRA_SYMBOL) === -1) {
      Object.defineProperty(object, JSONAPI_EXTRA_SYMBOL, {
        enumerable  : false,
        configurable: false,
        writable    : false,
        value       : {}
      });
    }

    let previousValues = object[JSONAPI_EXTRA_SYMBOL];

    Object
      .keys(values)
      .forEach((key: string) => {
        if (isPlainObject(values[key])) {
          object[JSONAPI_EXTRA_SYMBOL][key] = mergeWith({}, previousValues[key], values[key], (objValue, srcValue) => {
            return (isPlainObject(objValue))
                   ? undefined // Default merging by lodash method.
                   : srcValue; // Arrays, primitive types.
          });
        }
        else {
          object[JSONAPI_EXTRA_SYMBOL][key] = values[key];
        }
      });
  }
}
