export enum Stages {
  Development = 'dev',
  Local       = 'local',
  Production  = 'prod'
}
