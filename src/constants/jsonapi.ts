import { getExtras }         from '../classes/jsonapi-cyclic.class';
import { IGenericIdTextMap } from '../interfaces/generic.interfaces';

export const mediaType   = 'application/vnd.api+json';
export const contentType = `${mediaType}; charset=utf-8`;

export const statusCodes: IGenericIdTextMap<number> = {
  ok                  : {
    id  : 200,
    text: 'OK'
  },
  created             : {
    id  : 201,
    text: 'Created'
  },
  noContent           : {
    id  : 204,
    text: 'No Content'
  },
  badRequest          : {
    id  : 400,
    text: 'Bad Request'
  },
  forbidden           : {
    id  : 403,
    text: 'Forbidden'
  },
  notFound            : {
    id  : 404,
    text: 'Not Found'
  },
  notAcceptable       : {
    id    : 406,
    text  : 'Not Acceptable',
    detail: 'Media types parameters are not allowed.'
  },
  conflict            : {
    id  : 409,
    text: 'Conflict'
  },
  // See: https://stackoverflow.com/questions/16133923/400-vs-422-response-to-post-of-data/20215807#20215807
  unsupportedMediaType: {
    id    : 415,
    text  : 'Unsupported Media Type',
    detail: `The only supported media type is '${mediaType}'.`
  },
  unprocessableEntity : {
    id  : 422,
    text: 'Unprocessable Entity'
  },
  internalServerError : {
    id  : 500,
    text: 'Internal Server Error'
  },
  notImplemented      : {
    id  : 501,
    text: 'Not Implemented'
  }
};

export const defaultSerializerOptions = {
  id                    : 'id',
  blacklist             : [
    'type'
  ],
  blacklistOnDeserialize: [
    'id',
    'type',
    'createdAt',
    'updatedAt'
  ],
  topLevelMeta          : function (data, extraData) {
    let extras: any = getExtras(data);

    if (data && extras && extras.meta) {
      return extras.meta;
    }
  },
  topLevelLinks         : function (data, extraData) {
    let extras: any = getExtras(data);

    if (data && extras && extras.links) {
      return extras.links;
    }
  }
};

export enum EndpointTypes {
  Collection = 'collection',
  Resource   = 'resource',
  Action     = 'action'
}
