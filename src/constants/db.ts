import * as dynamoose from 'dynamoose';

export const ORM_SCHEMA_DEFAULT_OPTIONS: dynamoose.SchemaOptions & { errorUnknown: boolean } = {
  timestamps      : true,
  saveUnknown     : false,
  errorUnknown    : true,
  useDocumentTypes: true
};

export const defaultThroughput = {
  read : parseInt(process.env.SERVICE_DYNAMODB_TABLE_THROUGHPUT_READ),
  write: parseInt(process.env.SERVICE_DYNAMODB_TABLE_THROUGHPUT_WRITE)
};

export enum ValidationDirection {
  SchemaToData = 'schema-to-data',
  DataToSchema = 'data-to-schema'
}

export const ORM_SCHEMA_DEFAULT_PROPERTY_EXCLUSION_ATTRIBUTES_LIST = [
  'id',
  'type',
  'createdAt',
  'updatedAt'
];
