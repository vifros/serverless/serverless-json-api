export enum ErrorTypes {
  // Library.
  DbSchemaInitializationError     = 'DbSchemaInitializationError',

  // AWS DynamoDb.
  ConditionalCheckFailedException = 'ConditionalCheckFailedException',

  // JsonApi.
  JsonApiError                    = 'JsonApiError',
  JsonApiErrors                   = 'JsonApiErrors',
  JsonApiNotFoundError            = 'JsonApiNotFoundError',
  JsonApiServerError              = 'JsonApiServerError',
  JsonApiValidationError          = 'JsonApiValidationError',
  JsonApiValidationErrors         = 'JsonApiValidationErrors',
  JsonApiNotImplementedError      = 'JsonApiNotImplementedError',

  // Db.
  ValidationError                 = 'ValidationError',
  ValidationErrors                = 'ValidationErrors'
}

export enum ErrorTitles {
  // Generic.
  InvalidParameter        = 'Invalid Parameter.',
  InvalidRequest          = 'Invalid Request.',
  InvalidAttribute        = 'Invalid Attribute.',
  MaximumPageSizeExceeded = 'Page size requested is too large.',

  // Server Errors.
  NotImplemented          = 'Service not implemented yet.'
}

export enum ErrorMessages {
  // Db.
  InvalidSchema                                        = 'Invalid schema definition at `{source}`.',
  MissingRequiredTypeInSchemaAttribute                 = 'Attribute `type` parameter is required in schema definition for `{source}`.',
  UnsupportedTypeInSchemaAttribute                     = 'Unsupported `type` parameter at schema definition for `{source}`.',

  // JSON-API Requests.
  MissingDataMemberAtDocument                          = 'Missing required `data` member at document\'s top level.',
  MissingTypeMemberAtData                              = 'Missing required `type` member at document\'s `data`.',
  MissingIdMemberAtData                                = 'Missing required `id` member at document\'s `data`.',
  PresentIdMemberAtData                                = 'Client generated `id`s are not allowed.',
  ClientGeneratedIdAlreadyExists                       = 'Client generated `id` already exists.',
  InvalidType                                          = '`type` member doesn\'t constitute any of the recognized types allowed for the endpoint.',
  MissingAttributesMemberAtData                        = 'Missing required `attributes` member at document\'s `data`.',
  NotAllowedEmptyAttributesMember                      = '`attributes` members must contain at least one parameter.',
  IdAttributeNotAllowedForUpdate                       = '`id` attribute is not allowed for update.',
  IdMismatch                                           = '`id` from the request URI and the request body do not match.',

  // JSON-API Sort.
  MultipleSortParametersNotSupported                   = 'Multiple comma separated sorting parameters is not yet supported.',

  // JSON-API Pagination.
  RangePaginationNotSupported                          = 'Range pagination not supported.',
  InvalidPageSize                                      = '`page[size]` must be a positive integer; got `{got}.`',
  MaximumPageSizeExceeded                              = 'You requested a size of `{requestedPageSize}`, but `{maximumPageSize}` is the maximum.',

  // JSON-API Filter.
  BothAndOrConditionalFiltersCannotBePresent           = 'Both `$and` and `$or` cannot be specified at the same time.',
  BothFieldAndOrConditionalFiltersCannotBePresent      = 'Both a field and `$and` or `$or` cannot be specified at the same time.',
  MultipleConditionalFiltersForSameFieldIsNotSupported = 'Multiple conditional filters for the same field is not supported.',
  NoConditionalFiltersWereFoundInField                 = 'No conditional filters where found in the `{field}` field to filter for.',
  UnknownConditionalFilter                             = 'Unknown conditional filter: `{filter}`.',
  UnknownFieldToFilterFor                              = 'Unknown fieldname to filter for: `{field}`.',
  AtLeastOneConditionalFilterMustBePresent             = 'At least one conditional filter must be present.',

  // Validation.
  MissingRequiredAttribute                             = 'Missing required `{attribute}` attribute.',
  TimestampsAttributesCannotBeDirectlyModified         = 'Timestamps attributes like `createdAt` or `updatedAt` cannot be directly modified.',
  SchemaTypesMismatch                                  = 'Invalid `{attribute}` type, expecting a `{type}`.',
  AttributeIsReadOnly                                  = 'Attribute `{attribute}` is readonly and cannot be updated.',
  FailedEnumValidation                                 = 'Attribute `{attribute}` failed enum validation, expected one of `{enumValues}` and got `{value}`.',
  FailedValidation                                     = 'Attribute `{attribute}` failed validation `{validation}` with arguments `({args})`.',
  UnknownAttribute                                     = 'Unknown attribute `{attribute}`.',
  NotAllowedAttribute                                  = '`{attribute}` is present in the data and it is not allowed.',
  FailedCustomValidation                               = 'Attribute `{attribute}` failed custom validation `{validation}`.',
  UnknownValidation                                    = 'Unknown `{validation}`.',
  // Should this be a server error instead?
  FailedDefaultDefinitionExecution                     = 'Failed `{attribute}` attribute default definition execution.',

  // Server Errors.
  NotImplemented                                       = 'The requested service is planned, but not implemented yet.'
}
