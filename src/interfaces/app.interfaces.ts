import {
  APIGatewayEvent,
  Context
} from 'aws-lambda';

import {
  Request,
  Response
} from 'express';

import { ServerlessJsonApi } from '../classes/serverless-json-api.class';
import { EndpointTypes }     from '../constants/jsonapi';
import { HttpMethods }       from './http.interfaces';
import { IJsonApiResponse }  from './jsonapi.interfaces';

export interface IAppRequest extends Request {
  scope: IScope;
}

export interface IAppResponse extends Response {
}

export interface IScope {
  req: Request,
  serverlessJsonApi: ServerlessJsonApi;
  apiGateway: {
    event: APIGatewayEvent;
    context: Context;
  }
}

export interface IAppRouteConfig {
  method: HttpMethods;
  route: string;
  handler: IAppHandler;
  endpointType: EndpointTypes;
  relatedModelId: string;
}

export interface IAppHandler {
  // The last parameter is the `IAppHandlerMeta` object.
  (...params): Promise<IJsonApiResponse>;
}

export interface IAppHandlerMeta {
  isFirstInChain: boolean;
}
