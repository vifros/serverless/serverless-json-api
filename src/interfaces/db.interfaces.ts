import * as dynamoose from 'dynamoose';

export type IOrm = typeof dynamoose;

export type IDbModelLoader = (orm: IOrm) => void;

export interface IDbModelLoaderMap {
  id: string;
  tableName?: string;
  schema?: any;
  schemaOptions?: any;
  serializerOptions: any;
  metaOptions?: {
    allowClientGeneratedIds?: boolean;
  };
}

export type IOrmExecutedQueryMap = any[] & {
  lastKey: any;
}
