import { IGenericMap } from './generic.interfaces';

export interface IJsonApiErrorLinks {
  about?: string;
  type?: string | string[];
}

export interface IJsonApiErrorSource {
  pointer?: string;
  parameter?: string
}

export interface IJsonApiError {
  id?: string;
  links?: IJsonApiErrorLinks;
  status?: number;
  code?: string;
  title?: string;
  detail?: string;
  source?: IJsonApiErrorSource;
  meta?: IGenericMap;
}

export interface IJsonApiErrors {
  errors?: IJsonApiError[]
}

export interface IJsonApiErrorResponse extends IJsonApiErrors {
}

export interface IJsonApiResponse {
  status: number;
  headers?: {
    [index: string]: string;
  };
  data?: any // TODO @diosney: Change later to the interface for a proper JsonApi data response.
}
