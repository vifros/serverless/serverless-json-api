export interface IGenericIdTextMap<T_ID> {
  [index: string]: {
    id: T_ID;
    text: string;
    detail?: string;
  }
}

export interface IGenericMap {
  [index: string]: IGenericMap | any;
}
