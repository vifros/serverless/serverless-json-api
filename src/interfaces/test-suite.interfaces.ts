import { EndpointTypes } from '../constants/jsonapi';
import { TestTypes }     from '../constants/test-suite';
import { HttpMethods }   from './http.interfaces';

export interface TestItem {
  type: TestTypes;
  title: string;
  path?: string;
  method?: HttpMethods;
  endpointType?: EndpointTypes,
  children?: Set<TestItem>;
  expectations?: {
    status?: number;
    custom?: (expect: any, body: any) => void
  }
}
